package com.example;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@NamedNativeQueries({
	@NamedNativeQuery(name = "Shoes.byNameAndNum", query = "SELECT * FROM shoes "+
			" WHERE shoes.name = :name  AND shoes.num = :num", resultClass = Shoes.class)
	
})


@Table(name = "shoes")
class Shoes {	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@JsonProperty
	@Column(name = "name")
	private String name;
	
	@Column(name = "num")
	@JsonProperty
	private Integer num;
	
	@Column(name = "price")
	@JsonProperty
	private double price;
	
	//constructor
	protected Shoes(){}

	public Shoes(String name, Integer num, double price) {
		super();
		this.name = name;
		this.num = num;
		this.price = price;
	}
	
	//getters and setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
}
