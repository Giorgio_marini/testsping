package com.example;


import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;




@RestController
public class EndPoints {
	
	@Autowired
	private Shoes_Repository shoes_Repository;
	@Autowired
	private Clients_Repository clients_Reposity;
	
	@RequestMapping(path = "/add", method = RequestMethod.POST)
	public Shoes saveShoe(@RequestBody Shoes shoes){
		return shoes_Repository.save(shoes);
	}
	
	@RequestMapping(path = "/addClient", method = RequestMethod.POST)
	public Clients saveClient(@RequestBody Clients client){
		shoes_Repository.save(client.getShoes());
		return clients_Reposity.save(client);
	}
	@RequestMapping(path ="/allClient")
	public Set<Clients> getAllClient(){
		return clients_Reposity.findAll();
	}
	
	@RequestMapping(path ="/all")
	public Iterable<Shoes> getAllShoes(){
		return shoes_Repository.findAll();
	}
	@RequestMapping(path = "/findClientPer{ShoesName}" , method = RequestMethod.GET)
	public Set<Clients> retrivalClientPerShoes(@RequestParam String ShoesName){
		return clients_Reposity.findClientNameByShoesName(ShoesName);
	}
	
	@RequestMapping(path = "/findBy{name}" , method = RequestMethod.GET)
	public Set<Shoes> retrivalShoesByname(@RequestParam String name){
		return shoes_Repository.findByName(name);
	}
	
	@RequestMapping(path = "/findClientPerNum{num}", method = RequestMethod.GET)
	public Set<Clients> retrivalClientPerNum(@RequestParam Integer num){
		return clients_Reposity.findClientByShoesNum(num);
	}
	
	@RequestMapping(path = "/retrivalModel{num}" , method = RequestMethod.GET)
	public Set<Shoes> retrivalShoesBynum(@RequestParam Integer num){
		return shoes_Repository.findByNum(num);
	}
	
	@RequestMapping(path = "/by{name}{num}", method = RequestMethod.GET)
	public Set<Shoes> retrivalShoesByNameAndNum(@RequestParam String name, @RequestParam Integer num){
		return shoes_Repository.byNameAndNum(name,num);
	}
	
	
}
