package com.example;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
//@NamedNativeQueries({
//	@NamedNativeQuery(name = "Clients.findClientsByShoes", query = "SELECT Clients.name "+
//			"FROM Clients INNER JOIN clients_shoes ON Clients.id = clients_shoes.clients_id "+
//			"INNER JOIN shoes ON shoes.id = clients_shoes.shoes_id  WHERE shoes.name =: name  ", resultClass = Clients.class)
//
//})
@Table(name = "Clients")


public class Clients {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "name", unique = true)
	private String name;
	@JsonProperty
	@Column(name = "shoes_id")
	@OneToMany
	private List<Shoes> shoes;
	
	public Clients(String name, List<Shoes> shoes) {
		super();
		this.name = name;
		this.shoes = shoes;
	}
	protected Clients() {
		super();
	}	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Shoes> getShoes() {
		return shoes;
	}
	public void setShoes(List<Shoes> shoes) {
		this.shoes = shoes;
	}
	public void add(Shoes i){
		this.shoes.add(i);
	}
	public void remove(int i){
		if(i < shoes.size()){
			shoes.remove(i);
		}
	}

}
