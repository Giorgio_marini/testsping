package com.example;

import java.util.Set;

//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.query.Param;

public interface Clients_Repository extends CrudRepository<Clients, Long> {
	
	public Set<Clients> findAll();
	
	public Set<Clients> findClientNameByShoesName(String string);

	public Set<Clients> findClientByShoesNum(Integer num);
	
	
//	@Query(nativeQuery = true)
//	public Set<Clients> findClientByShoes(@Param("name") String name);

}
