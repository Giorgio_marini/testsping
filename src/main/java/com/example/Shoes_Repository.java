package com.example;


import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface Shoes_Repository extends CrudRepository<Shoes, Long> {
	
	public Set<Shoes> findByName(String name);
	
	public Set<Shoes> findByNum(Integer num);
	
	@Query(nativeQuery = true)
	public Set<Shoes> byNameAndNum(@Param("name") String name, @Param("num") Integer num);
	//public Set<Shoes> findByNameAndNum(@Param("name") String name, @Param("num") Integer num);
	
//	@Query("SELECT t.title FROM Todo t where t.id = :id") 
//    String findTitleById(@Param("id") Long id);
}
