App.service("connectorDB", function ($q,$http) {

    var ajax = function(method,url,data) {

        var deferred = $q.defer();
        var request = {
            method: method,
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        if (method === 'GET') {
            request.params = data;
        } else {
            request.data = data;
            console.log("data : " + data);
        }
        $http(request).then(function(response){
            deferred.resolve(response.data);
        },function(response, status){
            deferred.reject({data: response, status: status});
        });
        return deferred.promise;
    };

    this.getCustomers = function(params) {
        return ajax("GET","/all/", params);
    };

    this.getClient = function(params) {
        return ajax("GET", "/allClient", params);
    };
    
    this.saveCustomer = function(params) {
         return ajax("POST", "/add" , params);
    };
});