var App = angular.module("myApp",["ngRoute"]);

App.config(function($routeProvider) {
    $routeProvider
    .when("/" , { 
    	templateUrl : "html/home.html"
    })
    .when("/clients", { templateUrl: "html/client.html",
    	controller: "clientController"
    })
    .when("/customers" , { templateUrl: "html/customers.html",
    	controller: "customerController"
    });    
});